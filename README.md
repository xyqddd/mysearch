# mysearch

#### 介绍
使用纯java语言开发的根据文件名及内容进行快速搜索的桌面小程序。

#### 软件架构
软件架构说明
1.工具会对电脑上非系统文件夹上的（txt，word，excel，pdf，ppt）等十几种的可读文件进行快速全盘扫描，并对文件名及文件内容进行索引。
2.对于电脑上修改及删除的文件，工具会自动差异化更新维护索引库，用户无需操心。
3.工具会对文本类型文件自动做文件编码识别，准确率95%，无需用户操心。
3.用户无需配置任何分词词库，只要有的就能搜。
4.检索速度极快，基本达到0延迟，秒出结果。文件名及文件内容关键词实时高亮。
5.本工具主要采用，lucene+javafx+poi编写。


#### 安装教程

1. 只需要电脑上安装一个jre8即可运行。


#### 使用说明

1. 点击“点我运行.jar”即可运行。




