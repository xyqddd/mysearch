package com.xyq.fs.views;

import java.nio.file.Path;

import com.xyq.fs.constants.ParseConstant;
import com.xyq.fs.contants.filesuffixes2.ExcelEnums;
import com.xyq.fs.contants.filesuffixes2.PdfEnums;
import com.xyq.fs.contants.filesuffixes2.PptEnums;
import com.xyq.fs.contants.filesuffixes2.TxtEnums;
import com.xyq.fs.contants.filesuffixes2.WordEnums;
import com.xyq.fs.index.IndexFileInf;
import com.xyq.fs.util.MyHighLight;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;

import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

public class WindowsC extends Application {

	private HTMLEditor htmlEditor = new HTMLEditor();

	static ImageCursor Mouse = WindowsGo.Mouse;
	private Stage stage;
	private boolean canShow = true;
	
	

	public boolean isCanShow() {
		return canShow;
	}

	public void setCanShow(boolean canShow) {
		this.canShow = canShow;
	}

	private Path path;

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void show2() {

		Platform.runLater(() -> {
			stage.show();
		});
	}

	public void show(String [] searchKeys) {

		Platform.runLater(() -> {
			if(canShow)
			stage.show();
			stage.setTitle(path.getFileName() + "(内容预览)");
		});
		new Thread(() -> {
			String s = yuLanFile(path,searchKeys);
			Platform.runLater(() -> {

				if (s == null || s.trim().length() == 0)
					htmlEditor.setHtmlText("抱歉,无法读取该文档的内容,请手动点击查看");
				else
					htmlEditor.setHtmlText(s);
			});
		}).start();
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	@Override
	public void start(Stage stage) {

		this.stage = stage;
		VBox root = new VBox();
		Scene scene = new Scene(root);
		root.getChildren().addAll(htmlEditor);
		root.setPadding(new Insets(8, 8, 8, 8));
		root.setSpacing(5);
		root.setAlignment(Pos.BOTTOM_LEFT);
		stage.setWidth(770);
		stage.setHeight(550);
		stage.setScene(scene);

		Platform.runLater(() -> {
			htmlEditor.setHtmlText("内容加载中,请稍后...");
		});

		stage.getIcons().add(WindowsGo.ICON);

		// 在scene中修改光标
		scene.setCursor(Mouse);

	}
	
	/**
	 * 高亮算法
	 * 
	 * @param str
	 * @param key
	 * @return
	 */

	public String yuLanFile(Path path,String [] searchKeys) {

		String fname = path.getFileName().toString().toUpperCase();
		IndexFileInf iff = null;
		if (TxtEnums.suffixContains(fname)) {
			iff = ParseConstant.itf;
		} else if (fname.endsWith(PdfEnums.PDF)) {
			iff = ParseConstant.ipf;
		} else if (PptEnums.suffixContains(fname)) {
			iff = ParseConstant.ippf;
		} else if (WordEnums.suffixContains(fname)) {
			iff = ParseConstant.iwf;
		} else if (ExcelEnums.suffixContains(fname)) {
			iff = ParseConstant.ief;
		}
		if (iff != null)
			return MyHighLight.high(iff.getContent(path.toFile(), fname), searchKeys,
					"<span style=\"color:red\">", "</span>");
		return "抱歉,非可读文件的内容暂时不可预览!!!";
	}

	public static void main(String[] args) {
		launch(args);
	}
}