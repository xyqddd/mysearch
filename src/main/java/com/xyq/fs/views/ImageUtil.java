package com.xyq.fs.views;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

public class ImageUtil {

	private static String ROOT_SRC = System.getProperty("user.dir") + "\\CONF\\";

	public static Background getImgUrl(String name) {

		BackgroundImage myBI = null;
		myBI = new BackgroundImage(getImage(name), BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
				BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
		
		return new Background(myBI);

	}

	public static Image getImage(String name) {

		try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(ROOT_SRC + name + "\\"), 10240);) {
			return new Image(bis);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
