package com.xyq.fs.task;

import java.io.File;

import com.xyq.fs.index.IndexFileInf;

public class MyTask extends AfAsyncTask {

	private IndexFileInf iff;

	private String upFileName;

	private String suffix;

	private File file;

	public IndexFileInf getIff() {
		return iff;
	}

	public void setIff(IndexFileInf iff) {
		this.iff = iff;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getUpFileName() {
		return upFileName;
	}

	public void setUpFileName(String upFileName) {
		this.upFileName = upFileName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public MyTask(IndexFileInf iff, File file, String upFileName, int tempnum, boolean needUpateDoc) {

		this.iff = iff;
		this.file = file;

		this.upFileName = upFileName;

	}

	@Override
	protected void doInBackground() throws Exception {

		iff.indexFile(file, upFileName, suffix);
		file = null;
		upFileName = null;
		suffix = null;
	}

	@Override
	protected void onPostExecute() {

	}
}
