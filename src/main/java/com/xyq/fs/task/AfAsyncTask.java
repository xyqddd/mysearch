package com.xyq.fs.task;



public abstract class AfAsyncTask extends Thread {




	public void execute() {
		this.start();
	}

	@Override
	public void run() {

		try {
			doInBackground();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected abstract void doInBackground() throws Exception;

	protected abstract void onPostExecute();

}
