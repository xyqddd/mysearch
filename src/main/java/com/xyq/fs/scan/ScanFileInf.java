package com.xyq.fs.scan;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.xyq.fs.views.WindowsGo;

public class ScanFileInf {

	public static String ROOT_NAME;

	public long scan() {

		ExecutorService pool = Executors.newFixedThreadPool(1);
		ScanFileImpl sf = new ScanFileImpl(pool);
		WindowsGo.SLEEP_NOW = 4;
		WindowsGo.SLEEP_NOW = 1;

		long startTime = new Date().getTime();
		for (File f : WindowsGo.DIRROOTS) {
			try {
				ROOT_NAME = f.toString().replaceAll(":\\\\", "");
				Files.walkFileTree(f.toPath(), sf);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.gc();
		}
		long cha = new Date().getTime() - startTime;
		try {
			pool.shutdown();
			pool.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
			pool.shutdownNow();
			pool = null;
		}
		pool = null;
		WindowsGo.writerSkip();
	
		sf = null;
		return cha;
	}

	public static void main(String[] args) {

		for (File f : File.listRoots()) {
			System.out.println(f.toString().replaceAll(":\\\\", ""));

		}
	}
}
