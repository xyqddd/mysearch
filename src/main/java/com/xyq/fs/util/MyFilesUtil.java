package com.xyq.fs.util;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import java.io.File;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

public class MyFilesUtil {

	/**
	 * 拷贝文件
	 * 
	 * @param FilePath
	 */
	public void copeFile(final String FilePath) {

		Transferable t = new Transferable() {
			// 返回对象一定是要一个集合
			@Override
			public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				String[] p = { FilePath };
				List<File> l = new ArrayList<>();
				for (String str : p) {
					l.add(new File(str));
				}
				return l;
			}

			@Override
			public DataFlavor[] getTransferDataFlavors() {

				DataFlavor[] d = new DataFlavor[] { DataFlavor.javaFileListFlavor };

				return d;
			}

			@Override
			public boolean isDataFlavorSupported(DataFlavor flavor) {
				boolean b = DataFlavor.javaFileListFlavor.equals(flavor);

				return b;
			}
		};
		// Put the selected files into the system clipboard
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(t, null);

		// 返回指定flavor类型的数据
	}

	public static String getAbsPath(String filePath) {

		String path = null;
		path = System.getProperty("user.dir") + File.separator + "conf" + File.separator + filePath;
		return path;
	}
	
	public static void delDir(String dirPath) {

		File dir = new File(dirPath);
		File[] dirList = dir.listFiles();
		if (dirList == null || dirList.length == 0)
			dir.delete();
		else {
			// 删除所有文件
			for (File f : dirList)
				if (f.isDirectory())
					delDir(f.getAbsolutePath());
				else
					f.delete();
			// 删除完当前文件夹下所有文件后删除文件夹
			dirList = dir.listFiles();
			if (dirList.length == 0)
				dir.delete();
		}
	}

	public static void main(String[] args) {

		delDir("C:\\Users\\xyqq\\Desktop\\hqh照片\\");
		delDir("F:\\百度云备份\\本地备份\\aa\\");
	}
}
