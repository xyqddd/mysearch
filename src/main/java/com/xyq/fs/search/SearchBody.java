package com.xyq.fs.search;

import java.util.Date;

public class SearchBody {

	private String searchKey = "";
	private String[] searchKeys = new String[] {};
	private String[] noSearchKeys;
	private String noSearchKey = "";
	private String cachePre;
	private String cboxaValue;
	private Date startDate;
	private Date endDate;
	private String sb;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCboxaValue() {
		return cboxaValue;
	}

	public void setCboxaValue(String cboxaValue) {
		this.cboxaValue = cboxaValue;
	}

	public String[] getSearchKeys() {
		return searchKeys;
	}

	public void setSearchKeys(String[] searchKeys) {
		this.searchKeys = searchKeys;
	}

	public String[] getNoSearchKeys() {
		return noSearchKeys;
	}

	public void setNoSearchKeys(String[] noSearchKeys) {
		this.noSearchKeys = noSearchKeys;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getNoSearchKey() {
		return noSearchKey;
	}

	public void setNoSearchKey(String noSearchKey) {
		this.noSearchKey = noSearchKey;
	}

	public String getCachePre() {
		return cachePre;
	}

	public void setCachePre(String cachePre) {
		this.cachePre = cachePre;
	}

	public String getSb() {
		return sb;
	}

	public void setSb(String sb) {
		this.sb = sb;
	}

}
