package com.xyq.fs.search;

import org.apache.lucene.search.Query;

public class MySearchBean {

	private SearchBody body;
	private Query query;

	public SearchBody getBody() {
		return body;
	}

	public void setBody(SearchBody body) {
		this.body = body;
		if (body != null) {
				this.query = MySearch.buildQuery(body);
		} else
			this.query = null;
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

}
