package com.xyq.fs.contants.filesuffixes2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordEnums {

	private static final List<String> WORD_SET = new ArrayList<>();
	public static final String DOC = ".DOC";
	public static final String DOCX = ".DOCX";
	public static final String WPS = ".WPS";
    public static Set<String> mySet = new HashSet<String>();
	
	static {
		WORD_SET.add(".DOCX");
		WORD_SET.add(".DOC");
		WORD_SET.add(".WPS");
		
		mySet.addAll(WORD_SET);
	}

	public static boolean contains(String suffix) {

		return mySet.contains(suffix);
	}

	public static boolean suffixContains(String suffix) {

		for (String s : WORD_SET) {
			if (suffix.endsWith(s))
				return true;
		}
		return false;
	}

	public static List<String> getValues() {

		return WORD_SET;
	}
}
