package com.xyq.fs.contants.filesuffixes2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



public class TxtEnums {

	public static final List<String> TXT_SET = new ArrayList<>();
	private static final Set<String> SIMPLE_TXT_SET = new HashSet<>();
	public static Set<String> RUN_TXT_SET = new HashSet<>();
    
	public static Set<String> myset = new HashSet<String>();
	static {
		TXT_SET.add(".TXT");
		TXT_SET.add(".JAVA");
		TXT_SET.add(".JS");
		TXT_SET.add(".SQL");
		TXT_SET.add(".CSS");
		TXT_SET.add(".HTML");
		TXT_SET.add(".PY");
		TXT_SET.add(".CPP");
		TXT_SET.add(".HTML");
		TXT_SET.add(".XML");
		TXT_SET.add(".PHP");
		TXT_SET.add(".JSP");
		TXT_SET.add(".CSV");
		
		myset.addAll(TXT_SET);

		SIMPLE_TXT_SET.add(".TXT");
		RUN_TXT_SET = SIMPLE_TXT_SET;
	}
	
	public static void setRUN_TXT_SET(int n){
		
		if(n == 0)
			RUN_TXT_SET = SIMPLE_TXT_SET;
		else RUN_TXT_SET = myset;
	}

	public static boolean suffixContains(String suffix) {

		for (String s : RUN_TXT_SET) {
			if (suffix.endsWith(s))
				return true;
		}
		return false;
	}

	public static List<String> getValues() {

		return TXT_SET;
	}
	
	public static Set<String> getSimleValues() {

		return SIMPLE_TXT_SET;
	}

}
