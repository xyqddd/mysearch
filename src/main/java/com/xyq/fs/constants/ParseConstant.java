package com.xyq.fs.constants;


import java.util.HashMap;
import java.util.Map;

import com.xyq.fs.contants.filesuffixes2.ExcelEnums;
import com.xyq.fs.contants.filesuffixes2.OtherEnums;
import com.xyq.fs.contants.filesuffixes2.PptEnums;
import com.xyq.fs.contants.filesuffixes2.TxtEnums;
import com.xyq.fs.contants.filesuffixes2.WordEnums;
import com.xyq.fs.index.IndexDir;
import com.xyq.fs.index.IndexExcelFiles;
import com.xyq.fs.index.IndexFileInf;
import com.xyq.fs.index.IndexFileName;
import com.xyq.fs.index.IndexPdfFiles;
import com.xyq.fs.index.IndexPptFiles;
import com.xyq.fs.index.IndexTxtFiles;
import com.xyq.fs.index.IndexWordFiles;

public class ParseConstant {

	public static IndexTxtFiles itf = new IndexTxtFiles();
	public static IndexWordFiles iwf = new IndexWordFiles();
	public static IndexExcelFiles ief = new IndexExcelFiles();
	public static IndexPdfFiles ipf = new IndexPdfFiles();
	public static IndexPptFiles ippf = new IndexPptFiles();
	public static IndexFileName ifn = new IndexFileName();
	public static IndexDir idn = new IndexDir();

	public static Map<String, IndexFileInf> PARSE_MAP = new HashMap<String, IndexFileInf>();
	public static Map<String, IndexFileInf> PARSE_MAP_SIMPLE = new HashMap<String, IndexFileInf>();
	public static Map<String, IndexFileInf> PARSE_MAP_OTHER = new HashMap<String, IndexFileInf>();

	public static void init(){

	}
	static {
		TxtEnums.TXT_SET.forEach(t -> {
			PARSE_MAP.put(t, itf);
		});
		PptEnums.getValues().forEach(p -> {
			PARSE_MAP.put(p, ippf);
		});
		WordEnums.getValues().forEach(w -> {
			PARSE_MAP.put(w, iwf);
		});
		ExcelEnums.getValues().forEach(e -> {
			PARSE_MAP.put(e, ief);
		});
		PARSE_MAP.put(".PDF", ipf);

		OtherEnums.getValues().forEach(o -> {
			PARSE_MAP.put(o, ifn);
		});

		// --------------------------
		PptEnums.getValues().forEach(p -> {
			PARSE_MAP_SIMPLE.put(p, ippf);
		});
		WordEnums.getValues().forEach(w -> {
			PARSE_MAP_SIMPLE.put(w, iwf);
		});
		ExcelEnums.getValues().forEach(e -> {
			PARSE_MAP_SIMPLE.put(e, ief);
		});
		PARSE_MAP_SIMPLE.put(".PDF", ipf);
		PARSE_MAP_SIMPLE.put(".TXT", itf);
		
		//-----------------------
		OtherEnums.getValues().forEach(o -> {
		PARSE_MAP_OTHER.put(o, ifn);
	});
	}
}
