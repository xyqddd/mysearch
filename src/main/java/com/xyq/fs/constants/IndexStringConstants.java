package com.xyq.fs.constants;

import java.util.ArrayList;
import java.util.List;

import com.xyq.fs.contants.filesuffixes2.ExcelEnums;
import com.xyq.fs.contants.filesuffixes2.PdfEnums;
import com.xyq.fs.contants.filesuffixes2.PptEnums;
import com.xyq.fs.contants.filesuffixes2.TxtEnums;
import com.xyq.fs.contants.filesuffixes2.WordEnums;

public class IndexStringConstants {

	public static final String YSM = "已扫描的文件数量:";
	public static final String QBLX = "全部类型";
	public static final String SYPF = "所有盘符";
	public static final String WBLX = "文本类型";
	public static final String ASJPX = "按时间排序";
	public static final String WJMPX = "文件名排序";
	public static final String QT = "其它文件";
	public static final String QT2 = "文件夹";
	public static final String LASTACCTIME = "basic:lastAccessTime";
	public static  String DIAN = ".";
	public static final String MAOHAO = ":";


	public static List<String> suffixList = new ArrayList<>();

	static {
		suffixList.addAll(TxtEnums.getValues());
		suffixList.addAll(ExcelEnums.getValues());
		suffixList.addAll(WordEnums.getValues());
		suffixList.addAll(PptEnums.getValues());
		suffixList.add(PdfEnums.PDF);
	}

	public static List<String> simpleSuffixList = new ArrayList<>();

	static {
		simpleSuffixList.add(".TXT");
		simpleSuffixList.addAll(ExcelEnums.getValues());
		simpleSuffixList.addAll(WordEnums.getValues());
		simpleSuffixList.addAll(PptEnums.getValues());
		simpleSuffixList.add(PdfEnums.PDF);
	}
}
