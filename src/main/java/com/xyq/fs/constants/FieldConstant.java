package com.xyq.fs.constants;

public class FieldConstant {

	/** 文件名字段 */
	public static final String  FILE_NAME = "FILE_NAME";
	/** 文件路径字段 */
	public static String FILE_PATH = "FILE_PATH";
	/** 文件路径存储字段 */
	public static String FILE_PATH_INDEX = "FILE_PATH_INDEX";
	
	/** 文件名后缀字段 */
	public static String FILE_SUFFIX = "FILE_SUFFIX";
	/** 文件修改时间字段 */
	public static String FILE_UPDATE_TIME = "FILE_UPDATE_TIME";
	/** 文件备注排序字段 */
	public static String FILE_BZ_SORT = "FILE_BZ_SORT";
	/** 文件备注二进制字段 */
	public static String FILE_BZ_SORT2 = "FILE_BZ_SORT2";
	/** 文件备注字段3 */
	public static String FILE_BZ_SORT3 = "FILE_BZ_SORT3";
	public static String B = "b";
	/** 文件内容字段 */
	public static String FILE_CONTENT = "FILE_CONTENT";
	
	/** 盘符字段 */
	public static String FILE_PANFU = "FILE_PANFU";

	
}
