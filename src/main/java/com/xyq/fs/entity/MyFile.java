package com.xyq.fs.entity;

import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;

public class MyFile {

    private ScoreDoc afterScoreDoc;
	private int pageSize = 5;
	private int maxPageNum;
	private int totalResultNum = 0;
	private List<Document> docList = new ArrayList<>();
	public List<Document> getDocList() {
		return docList;
	}

	public void setDocList(List<Document> docList) {
		this.docList = docList;
	}

	public int getTotalResultNum() {
		return totalResultNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getMaxPageNum() {
		return maxPageNum;
	}

	public void setMaxPageNum(int maxPageNum) {
		this.maxPageNum = maxPageNum;
	}

	public ScoreDoc getAfterScoreDoc() {
		return afterScoreDoc;
	}

	public void setAfterScoreDoc(ScoreDoc afterScoreDoc) {
		this.afterScoreDoc = afterScoreDoc;
	}

	public void setTotalResultNum(int totalResultNum) {

		this.totalResultNum = totalResultNum;
		if (totalResultNum > 0 && totalResultNum < pageSize)
			this.maxPageNum = 1;
		else
			// 设置最大页数
			this.maxPageNum = totalResultNum % pageSize == 0 ? totalResultNum / pageSize
					: (totalResultNum / pageSize) + 1;

	}

}
