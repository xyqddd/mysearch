package com.xyq.fs.index;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.Field.Store;

import org.apache.lucene.document.NumericDocValuesField;

import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

import org.apache.poi.extractor.POITextExtractor;

import org.apache.poi.ooxml.extractor.ExtractorFactory;

import org.apache.poi.sl.extractor.SlideShowExtractor;

import org.apache.poi.xslf.usermodel.XMLSlideShow;

import com.xyq.fs.constants.FieldConstant;
import com.xyq.fs.contants.filesuffixes2.PptEnums;
import com.xyq.fs.scan.ScanFileInf;
import com.xyq.fs.views.WindowsGo;

public interface IndexFileInf {

	static Document doc = new Document();
	static StringField sf = new StringField(FieldConstant.FILE_PATH, "", Store.YES);
	static StringField sf2 = new StringField(FieldConstant.FILE_PANFU, "", Store.NO);
	static StringField sf3 = new StringField(FieldConstant.FILE_SUFFIX, "", Store.NO);
	static TextField tf = new TextField(FieldConstant.FILE_NAME, "", Store.YES);

	static NumericDocValuesField nvf = new NumericDocValuesField(FieldConstant.FILE_UPDATE_TIME, 0l);
	static StoredField sdf = new StoredField(FieldConstant.FILE_UPDATE_TIME, 0l);

	static LongPoint lp = new LongPoint(FieldConstant.FILE_UPDATE_TIME, 0l);

	public default Document getSimDoc(File file, String fileName, String suffix) {

		doc.clear();

		// 文件路径
		sf.setStringValue(file.getAbsolutePath());
		doc.add(sf);
		// 盘符
		if (ScanFileInf.ROOT_NAME != null)
			sf2.setStringValue(ScanFileInf.ROOT_NAME);
		else
			sf2.setStringValue(file.getAbsolutePath().charAt(0) + "");

		doc.add(sf2);

		// 文件后缀

		sf3.setStringValue(suffix);
		doc.add(sf3);
		// 文件名
		fileName = fileName.replaceAll("\\s+", "");
		tf.setStringValue(fileName);

		doc.add(tf);

		long ft = file.lastModified();
		nvf.setLongValue(ft);
		sdf.setLongValue(ft);
		lp.setLongValue(ft);

		doc.add(nvf);
		doc.add(sdf);
		doc.add(lp);

		fileName = null;
		suffix = null;
		file = null;

		return doc;
	}

	public default String getContent(File file, String upFileName) {

		String content = "抱歉,文件暂时无法预览";
		POITextExtractor extractor = null;
		try (InputStream bis = new FileInputStream(file);) {

			if (upFileName.endsWith(PptEnums.PPTX)) {
				content = readPptx(file);
			} else {
				extractor = ExtractorFactory.createExtractor(bis);
				content = extractor.getText();
			}
		
		} catch (Exception e) {
			WindowsGo.skipFilesMap.add(file.getAbsolutePath());
			System.out.println("inf无法解析:" + file);
			System.out.println("长度为：" + WindowsGo.skipFilesMap.size());
		} finally {
			if (extractor != null)
				try {
					extractor.close();
				} catch (IOException e) {
					extractor = null;
				}
			file = null;
			extractor = null;
			upFileName = null;
		}
		try {
			return content;
		} catch (Exception e) {
		} finally {
			content = null;
		}
		return content;
	}

	default String readPptx(File file) {

		String text2007 = "抱歉，文件暂时无法预览";
		try (FileInputStream fis = new FileInputStream(file);
				SlideShowExtractor ss = new SlideShowExtractor(new XMLSlideShow(fis));) {
			text2007 = ss.getText();
		} catch (Exception e) {
		}

		return text2007;
	}

	public void indexFile(File file, String upFileName, String suffix);

	public static void main(String[] args) {

	}

}
