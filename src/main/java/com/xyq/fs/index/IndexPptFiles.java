package com.xyq.fs.index;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import com.xyq.fs.base.MyDirectoryS;
import com.xyq.fs.constants.FieldConstant;


public class IndexPptFiles implements IndexFileInf {

	@Override
	public void indexFile(File file, String upFileName, String suffix) {

		Document doc = null;
		String content = "";

		doc = getSimDoc(file, upFileName, suffix);

		try {
			content = getContent(file, upFileName);
			//content = content.replaceAll("\\s+|\t|\n", "");
		} catch (Exception e) {
			System.out.println("无法解析" + file);
		} finally {
			TextField tf = new TextField(FieldConstant.FILE_CONTENT, content, Store.NO);
			doc.add(tf);
			MyDirectoryS.addDocument(doc);
			tf = null;

			content = null;
			upFileName = null;
			file = null;
		}
	}

	public static void main(String[] args) throws IOException {

	}
}
