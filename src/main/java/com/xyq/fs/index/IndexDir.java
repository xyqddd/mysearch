package com.xyq.fs.index;

import java.io.File;

import org.apache.lucene.document.Document;

import com.xyq.fs.base.MyDirectoryS;
import com.xyq.fs.constants.IndexStringConstants;

public class IndexDir implements IndexFileInf {

	@Override
	public void indexFile(File file, String upFileName, String suffix) {

		Document doc = null;
		doc = getSimDoc(file, upFileName, IndexStringConstants.QT2);
		MyDirectoryS.addDocument(doc);

		file = null;
		upFileName = null;
		suffix = null;
		doc = null;
	}

}
