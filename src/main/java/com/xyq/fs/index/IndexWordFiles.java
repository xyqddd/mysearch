package com.xyq.fs.index;

import java.io.File;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;

import com.xyq.fs.base.MyDirectoryS;
import com.xyq.fs.constants.FieldConstant;

public class IndexWordFiles implements IndexFileInf {

	@Override
	public void indexFile(File file, String upFileName, String suffix) {

		Document doc = null;
		String content = "";

		doc = getSimDoc(file, upFileName, suffix);

		if (doc != null) {
			content = getContent(file, upFileName);
			TextField tf = new TextField(FieldConstant.FILE_CONTENT, content, Store.NO);
			doc.add(tf);
			tf = null;
		}
		MyDirectoryS.addDocument(doc);

		upFileName = null;
		content = null;
		file = null;
		doc = null;

	}

	public static void main(String[] args) {

		File f = new File("d:\\新.docx");
		System.out.println(f.length());
	}

}
