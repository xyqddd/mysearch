package com.xyq.fs.index;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.TextField;

import org.apache.lucene.document.Field.Store;

import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.xyq.fs.base.MyDirectoryS;
import com.xyq.fs.constants.FieldConstant;
import com.xyq.fs.contants.filesuffixes2.ExcelEnums;
import com.xyq.fs.views.WindowsGo;

public class IndexExcelFiles implements IndexFileInf {

	private static Map<String, ExcelTypeEnum> map = new HashMap<String, ExcelTypeEnum>();

	static {
		map.put(".XLS", ExcelTypeEnum.XLS);
		map.put(".ET", ExcelTypeEnum.XLS);
		map.put(".XLSX", ExcelTypeEnum.XLSX);
	}

	@Override
	public void indexFile(File file, String upFileName, String suffix) {

		ExcelTypeEnum en = null;
		Document doc = getSimDoc(file, upFileName, suffix);
		en = map.get(suffix);

		upFileName = getContent2(file, en);
		upFileName = upFileName.replaceAll("\\s+|\t|\n", "");
		TextField tx = new TextField(FieldConstant.FILE_CONTENT, upFileName, Store.NO);
		doc.add(tx);
		MyDirectoryS.addDocument(doc);

		tx = null;
		upFileName = null;
		en = null;
		doc = null;
		suffix = null;
		file = null;

	}

	public String getContent(File file) {

		ExcelTypeEnum en = null;

		String ps = file.getAbsolutePath().toUpperCase();
		if (ps.endsWith(ExcelEnums.XLS)) {

			en = ExcelTypeEnum.XLS;
		} else if (ps.endsWith(ExcelEnums.XLSX)) {
			en = ExcelTypeEnum.XLSX;
		} else if (ps.endsWith(ExcelEnums.ET)) {
			en = ExcelTypeEnum.XLS;
		}
		return getContent2(file, en);
	}

	public String getContent2(File file, ExcelTypeEnum en) {

		if (en == null) {
			String ps = file.getAbsolutePath().toUpperCase();
			if (ps.endsWith(ExcelEnums.XLS)) {

				en = ExcelTypeEnum.XLS;
			} else if (ps.endsWith(ExcelEnums.XLSX)) {
				en = ExcelTypeEnum.XLSX;

			} else if (ps.endsWith(ExcelEnums.ET)) {
				en = ExcelTypeEnum.XLS;
			}
		}
		StringBuilder sb = new StringBuilder();
		try (InputStream inputStream = new FileInputStream(file);) {

			@SuppressWarnings("deprecation")
			ExcelReader reader = new ExcelReader(inputStream, en, null, new AnalysisEventListener<List<String>>() {
				@Override
				public void invoke(List<String> object, AnalysisContext context) {

					if (object.size() > 0) {
						for (String s : object) {
							if (s != null && s.trim().length() > 0)
								sb.append(s).append("\t");
						}
						sb.append("\n");
					}
					object = null;
					context = null;
				}

				@Override
				public void doAfterAllAnalysed(AnalysisContext context) {
					context = null;
				}
			});
			reader.read();

		} catch (Exception e) {

			WindowsGo.skipFilesMap.add(file.getAbsolutePath());
			System.out.println("无法解析" + file);
		}
		return sb + "";
	}

	public static void main(String[] args) throws IOException {

	}
}
