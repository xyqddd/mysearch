package com.xyq.fs.index;

import java.io.File;

import org.apache.lucene.document.Document;

import com.xyq.fs.base.MyDirectoryS;
import com.xyq.fs.constants.IndexStringConstants;

public class IndexFileName implements IndexFileInf {

	@Override
	public void indexFile(File file, String upFileName, String suffix) {

		Document doc = getSimDoc(file, upFileName, IndexStringConstants.QT);
		MyDirectoryS.addDocument(doc);
		file = null;
		doc = null;
		upFileName = null;
		suffix = null;

	}

}
